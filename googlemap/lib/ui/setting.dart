import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SettingScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SettingScreenState();
  }

}

class SettingScreenState extends State<SettingScreen>{

  late LatLng userPosition;
  List<Marker> markers = [];

  @override
  void setState(VoidCallback fn) {
    
  }


  // @override
  // void initState(){
  //   getLocation();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: findUserLocation(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (snapshot.hasData){
            return GoogleMap(
              initialCameraPosition: CameraPosition(
                target: snapshot.data,
                zoom: 16
              ),
              markers: Set<Marker>.of(markers),
            );
          }
          else{
            return setLoading();
          }
        },
      )
    );
  }

  Widget setLoading(){
    return Scaffold(
      body: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Text('Waiting...', style: TextStyle(fontSize: 25, color: Colors.blueAccent),),
            SizedBox(height: 20,),
            CircularProgressIndicator()
          ],
        ),
      ),

    );
  }

  Future<LatLng> findUserLocation() async{
    Location location = Location();
    LocationData userLocation;
    PermissionStatus hasPermission = await location.hasPermission();
    bool active = await location.serviceEnabled();
    if(hasPermission == PermissionStatus.granted && active){
      userLocation = await location.getLocation();
      userPosition = LatLng(userLocation.latitude as double, userLocation.longitude as double);
    }
    else{
      userPosition = LatLng(10.735401059960099, 106.70139772658432);
    }

    if(markers.isEmpty){
      markers.add(buildMarker(userPosition));
    }
    else{
      markers[0] = buildMarker(userPosition);
    }
    setState(() {
      
    });
    return userPosition;
  }

  Marker buildMarker(LatLng pos){
    MarkerId markerId = MarkerId('H');
    Marker marker = Marker(markerId: markerId, position: pos);
    return marker;
  }

}
